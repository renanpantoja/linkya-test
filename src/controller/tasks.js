$(document).ready(function(){

  //create task
  $(document).on("submit",".create-task-form",function(e){
    e.preventDefault();

    var dados = $( this ).serializeArray();
    $(".create-task-form input[name*='name']").val("");
    // console.log(dados);

    $.post( window.location.href + 'backend/tasks/add', {dados}, function( result ) {
        alert('Create Task!');

        // console.log(result);

        $.get("src/view/task_template.html", function(data){
          data = data.replace(/444030/g, result.replace( /^\D+/g, ''));
          data = data.replace(/6602060330/g, dados[0].value);
          data = data.replace(/444022206660660/g, 'unchecked');
          data = data.replace('hide', '');
          // console.log(data);
          project_ul = '#tasks_content_' + dados[1].value;
          $(project_ul).append(data);
        });

    });

  });

  //conclude task
  $(document).on("click",".end_task_btn",function(e){

    task_id = $(this).attr('id').replace( /^\D+/g, '');
    task_content = '#task_' + task_id;
    date_now = ' - ' + new Date().toJSON().slice(0,10).replace(/-/g,'-')

    $(this).find('span').toggleClass('glyphicon-unchecked').toggleClass('glyphicon-check');
    $(task_content + ' .end').html(date_now);
    $(task_content + ' .pull-right').addClass('hide');

      $.post( window.location.href + 'backend/tasks/end', {task_id}, function( result ) {
        alert('Task complete!');
        // console.log(result);
      });

  });

  //delete task
  $(document).on("click",".delete_task",function(e){

    task_id = $(this).attr('id').replace( /^\D+/g, '');
    task_content = '#task_' + task_id;
    $(task_content).delay(100).fadeOut(function() { $(task_content).remove(); });
    // console.log(project_content);

    $.post( window.location.href + 'backend/tasks/delete', {task_id}, function( result ) {
      alert('Task delete!');
      // console.log(result);
    });

  });

});
