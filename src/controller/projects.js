$(document).ready(function(){

  //criando projeto
  $(document).on("submit","#create-project-form",function(e){
    e.preventDefault();

    var dados = $( this ).serializeArray();
    $("#create-project-form input[name*='name']").val("");
    // console.log(dados);

    $.post( window.location.href + 'backend/projects/add', {dados}, function( result ) {
        alert('project created!');

        // console.log(result);

        $.get("src/view/project_template.html", function(data){
            data = data.replace(/444030/g, result.replace( /^\D+/g, ''));
            data = data.replace(/6602060330/g, dados[0].value);
            $("#projects-content").append(data).fadeIn();
        });
    });

  });

  //excluindo projeto
  $(document).on("click",".delete_project",function(e){

    project_id = $(this).attr('id').replace( /^\D+/g, '');
    project_content = '#project_' + project_id;
    $(project_content).delay(100).fadeOut(function() { $(project_content).remove(); });
    // console.log(project_content);

    $.post( window.location.href + 'backend/projects/delete', {project_id}, function( result ) {
      // console.log(result);
    });

  });

  //edit project
  $(document).on("click",".edit_project",function(e){

    project_id = $(this).attr('id').replace( /^\D+/g, '');
    project_content = '#project_' + project_id;
    var project_new_name = prompt("Edit the project name", $(project_content + ' .project_name_content').html());
    if(project_new_name){
      $(project_content + ' .project_name_content').html(project_new_name);
      var dados = [project_new_name, project_id];
      // console.log(project_content);

      $.post( window.location.href + 'backend/projects/edit', {dados}, function( result ) {
        alert('Project edited!');
        // console.log(result);
      });
    }

  });

  function readProjects(user_id){
    $.post( window.location.href + 'backend/projects/read', {user_id}, function( result ) {
        user_projects = jQuery.parseJSON(result);
        // console.log(result);
        $("#projects-content").empty();
        $.each( user_projects, function( key, value ) {
          $.get("src/view/project_template.html", function(data){
              data = data.replace(/444030/g, value.projects_id);
              data = data.replace(/6602060330/g, value.title);
              $("#projects-content").append(data);
          });
        });

        $.post( window.location.href + 'backend/tasks/read', {user_id}, function( result ) {
          // console.log(result);
          var results = jQuery.parseJSON(result);
            $.each( results, function( key, value ) {
              $.get("src/view/task_template.html", function(data){
                data = data.replace(/444030/g, value.tasks_id);
                data = data.replace(/6602060330/g, value.description);
                if(value.end_date){ check = 'check'; } else { check = 'unchecked'; }
                data = data.replace(/444022206660660/g, check);
                // console.log(data);
                project_ul = '#tasks_content_' + value.project;
                $(project_ul).append(data);
              });
            });
        });
    });
  }

});
