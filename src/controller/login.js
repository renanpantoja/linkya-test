$(document).ready(function(){

  //cadastrando usuario
  $(document).on("submit","#register-form",function(e){
    e.preventDefault();

    var dados = $( this ).serializeArray();

    senha = dados[3].value;

    // console.log(dados);
    // console.log(senha);

    if(senha){

        $.ajax({
            type: "POST",
            url: window.location.href + 'backend/user/register',
            dataType: "json",
            data: {register_user: dados},
            success: function (result) {
                // console.log(result);
                user_id = result[2];
                if(result[0] == 1) {
                    alert(result[1]);
                    $('#register-modal').modal('toggle');
                    $('#login-modal').modal('toggle');
                }

            }
        });
    }

  });

  //logando com Usuário
  $(document).on("submit","#login-form",function(e){
      e.preventDefault();

      var dados = $("#login-form").serializeArray();

      $.post( window.location.href + 'backend/user/login', {dados}, function( result ) {
          user_data = jQuery.parseJSON(result);
          user_data.logado = true;
          user_id = user_data.user_id;
          user_id = user_id.replace( /^\D+/g, '');
          if($.isNumeric(result)){
            alert('Invalid user or password!');
          } else {
            // console.log(user_data);
            // console.log(typeof(user_data));
            $('#login-modal').modal('toggle');

            $('.login-options').hide();
            $('#login-template').hide();
            $('.loged-options').removeClass('hide');
            $('#projects-template').removeClass('hide');
            $('.user-id-field').val(user_id);

            readProjects(user_id);

          }
      });

  });

  //fazendo Logout
  $(document).on("click","#logout-button",function(e){
    $.post( window.location.href + 'backend/user/logout', function( result ) {
      if($.isNumeric(result)){
        window.location.reload()
      }
    });
  });

  function readProjects(user_id){
    $.post( window.location.href + 'backend/projects/read', {user_id}, function( result ) {
        user_projects = jQuery.parseJSON(result);
        // console.log(result);
        $("#projects-content").empty();
        $.each( user_projects, function( key, value ) {
          $.get("src/view/project_template.html", function(data){
              data = data.replace(/444030/g, value.projects_id);
              data = data.replace(/6602060330/g, value.title);
              $("#projects-content").append(data);
          });
        });

        $.post( window.location.href + 'backend/tasks/read', {user_id}, function( result ) {
          // console.log(result);
          var results = jQuery.parseJSON(result);
            $.each( results, function( key, value ) {
              $.get("src/view/task_template.html", function(data){
                data = data.replace(/444030/g, value.tasks_id);
                data = data.replace(/6602060330/g, value.description);
                if(value.end_date){
                  check = 'check';
                  data = data.replace('<span class="end"></span>', ' - '+value.end_date);
                } else {
                  check = 'unchecked';
                  data = data.replace('hide', '');
                }
                data = data.replace(/444022206660660/g, check);
                // console.log(data);
                project_ul = '#tasks_content_' + value.project;
                $(project_ul).append(data);
              });
            });
        });
    });
  }

});
