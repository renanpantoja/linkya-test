<?php

if($_SERVER['REQUEST_METHOD'] == 'POST'){

  $projects = new Projects(new Config());

    if($action == 'read'){

        echo json_encode($projects->readProjects($_POST['user_id']));

    } else if($action == 'add'){

      $falha = false;
      for ($c = 0; $c < count($_POST['dados']); $c++) {
          if($_POST['dados'][$c]['value'] == ''){
              $falha = true;
              $message[$c] = ucfirst($_POST['dados'][$c]['name']).' empty';
          } else {
              $dados[$c] = $_POST['dados'][$c]['value'];
          }
      }

      echo json_encode($projects->addProjects($dados));

    } else if($action == 'delete'){

      echo json_encode($projects->deleteProject($_POST['project_id']));

    } else if($action == 'edit'){

      echo json_encode($projects->editProject($_POST['dados']));

    }

}
