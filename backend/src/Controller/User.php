<?php

if($_SERVER['REQUEST_METHOD'] == 'POST'){

  $users = new Users(new Config());

    if($action == 'register'){

        $falha = false;
        for ($c = 0; $c < count($_POST['register_user']); $c++) {
            if($_POST['register_user'][$c]['value'] == ''){
                $falha = true;
                $message[$c] = ucfirst($_POST['register_user'][$c]['name']).' empty';
            } else {
                $dados[$c] = $_POST['register_user'][$c]['value'];
            }
        }

        if(!$falha){
            $pai = (int)$users->cadastrar($dados);
            echo json_encode(array('1', 'Success user register!',$pai));
        } else {
            echo json_encode(array('0', implode(', ', $message).'.'));
        }

    } else if($action == 'login'){

      $dados = array();
      $dados['usuario'] = $_POST['dados'][0]['value'];
      $dados['senha'] = $_POST['dados'][1]['value'];

      echo json_encode($users->login($dados));

    } else if($action == 'logout'){

      echo json_encode($users->logout());

    }

}
