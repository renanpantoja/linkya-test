<?php

if($_SERVER['REQUEST_METHOD'] == 'POST'){

  $tasks = new Tasks(new Config());

    if($action == 'read'){

        echo json_encode($tasks->readTasks($_POST['user_id']));

    } else if($action == 'add'){

      $falha = false;
      for ($c = 0; $c < count($_POST['dados']); $c++) {
          if($_POST['dados'][$c]['value'] == ''){
              $falha = true;
              $message[$c] = ucfirst($_POST['dados'][$c]['name']).' empty';
          } else {
              $dados[$c] = $_POST['dados'][$c]['value'];
          }
      }

      echo json_encode($tasks->addTasks($dados));

    } else if($action == 'end'){

      echo json_encode($tasks->endTask($_POST['task_id']));

    } else if($action == 'delete'){

      echo json_encode($tasks->deleteTask($_POST['task_id']));

    }

}
