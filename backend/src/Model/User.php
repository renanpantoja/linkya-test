<?php

class Users{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function login($dados=null){

        session_start();

        if(empty($dados)){
            if(!empty($_COOKIE['r_uryh']) and !empty($_COOKIE['r_oijd'])) {
                $dados['usuario'] = base64_decode($_COOKIE['r_uryh']);
                $dados['senha'] = base64_decode($_COOKIE['r_oijd']);
            }
        } else {

            $this->lembrar($dados);

        }

        if(isset($dados['usuario']) and isset($dados['senha'])) {

            $usuario = $this->retUsuario($dados['usuario']);
            if (password_verify($dados['senha'], $usuario['password'])) {
                $_SESSION['usuario'] = $usuario;
                return $_SESSION['usuario'];
            } else {
                return 0;
            }
        }

    }

    public function logout(){

        session_start();
        session_unset();
        session_destroy();
        setcookie('cursophp_uryh');
        setcookie('cursophp_oijd');
        return 0;

    }

    public function protege(){

        session_start();
        if(empty($_SESSION['usuario'])){
            header('Location: ../');
        }

    }

    public function lembrar($dados){

        $cookie = array(
            'usuario'=>base64_encode($dados['usuario']),
            'senha'=>base64_encode($dados['senha'])
        );

        setcookie('cursophp_uryh', $cookie['usuario'], (time() + (15*24*3600)), $_SERVER['SERVER_NAME']);
        setcookie('cursophp_oijd', $cookie['senha'], (time() + (15*24*3600)), $_SERVER['SERVER_NAME']);

    }

    public function hash($senha){

        return password_hash($senha, PASSWORD_BCRYPT, array('cost'=>12));

    }

    public function retUsuario($user){

        $usuario = $this->mysql->prepare('SELECT * FROM users WHERE usuario = :usuario');
        $usuario->bindValue(':usuario', $user, PDO::PARAM_STR);
        $usuario->execute();
        return $usuario->fetch();

    }

    //CRUD

    public function cadastrar($dados){

        $cadastra = $this->mysql->prepare('INSERT into users (nome, email, usuario, password) VALUES (:nome, :email, :usuario, :password);');
        $cadastra->bindValue(':nome', $dados[0], PDO::PARAM_STR);
        $cadastra->bindValue(':email', $dados[1], PDO::PARAM_STR);
        $cadastra->bindValue(':usuario', $dados[2], PDO::PARAM_STR);
        $cadastra->bindValue(':password', $this->hash($dados[3]), PDO::PARAM_STR);
        $cadastra->execute();
        return $this->mysql->lastInsertId();

    }

    public function deleteUsuario($id){
        $delete = $this->mysql->prepare('DELETE FROM users WHERE user_id = :user_id;');
        $delete->bindValue(':user_id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

}
