<?php

class Tasks{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function addTasks($dados){
      $cadastra = $this->mysql->prepare('INSERT into tasks (description, create_date, project) VALUES (:description, :create_date, :project);');
      $cadastra->bindValue(':description', $dados['0'], PDO::PARAM_STR);
      $cadastra->bindValue(':create_date', date('Y-m-d'), PDO::PARAM_STR);
      $cadastra->bindValue(':project', $dados['1'], PDO::PARAM_INT);

      if($cadastra->execute()){
        return $this->mysql->lastInsertId();
      } else {
        return false;
      }
    }

    public function readTasks($user){
        $tasks = $this->mysql->prepare("SELECT * FROM tasks INNER JOIN projects ON projects.projects_id = tasks.project WHERE projects.user = :user");
        $tasks->bindValue(':user', $user, PDO::PARAM_INT);
        $tasks->execute();
        return $tasks->fetchAll();
    }

    public function endTask($id){
        $tasks = $this->mysql->prepare("UPDATE tasks set end_date = :end_date where tasks_id=:tasks_id");
        $tasks->bindValue(':end_date', date('Y-m-d'), PDO::PARAM_STR);
        $tasks->bindValue(':tasks_id', $id, PDO::PARAM_INT);
        return $tasks->execute();
    }

    public function deleteTask($id){
  		$delete = $this->mysql->prepare('DELETE FROM tasks WHERE tasks_id = :tasks_id;');
  		$delete->bindValue(':tasks_id', $id, PDO::PARAM_INT);
  		return $delete->execute();
  	}

}
