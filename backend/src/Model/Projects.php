<?php

class Projects{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function addProjects($dados){
      $cadastra = $this->mysql->prepare('INSERT into projects (title, create_date, user) VALUES (:title, :create_date, :user);');
      $cadastra->bindValue(':title', $dados['0'], PDO::PARAM_STR);
      $cadastra->bindValue(':create_date', date('Y-m-d'), PDO::PARAM_STR);
      $cadastra->bindValue(':user', $dados['1'], PDO::PARAM_INT);

      if($cadastra->execute()){
        return $this->mysql->lastInsertId();
      } else {
        return false;
      }
    }

    public function readProjects($user){
      $select = $this->mysql->prepare('SELECT * FROM projects WHERE user = :user ORDER BY projects_id ASC;');
      $select->bindValue(':user', $user, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll();
    }

    public function deleteProject($id){
  		$delete = $this->mysql->prepare('DELETE FROM projects WHERE projects_id = :projects_id;');
  		$delete->bindValue(':projects_id', $id, PDO::PARAM_INT);
  		return $delete->execute();
  	}

    public function editProject($dados){
        $edit = $this->mysql->prepare("UPDATE projects set title = :title where projects_id=:projects_id");
        $edit->bindValue(':title', $dados[0], PDO::PARAM_STR);
        $edit->bindValue(':projects_id', $dados[1], PDO::PARAM_INT);
        return $edit->execute();
    }

}
