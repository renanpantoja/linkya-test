<?php

date_default_timezone_set('Europe/Lisbon');
header('Content-Type: text/html; charset=utf8');

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', pathinfo(__FILE__)['dirname']);

include 'lib/loader.php';

$loader = Loader::get_instance(ROOT, DS);
$loader->get('lib/Request');

$loader->get('config/Config');
$loader->get('config/Route');
// include 'src/Model/User.php';ss
// $users = new Users(new Config());
// $users->protege();

$protocol = 'http';

$rota = Route::dynamicUrl($protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 3);
define("URL_BASE", array_pop($rota));

$module = (empty($rota[0])) ? 'user' : ucfirst($rota[0]);
$action = (isset($rota[1]) && (!empty($rota[1]))) ? $rota[1] : 'user';
$param = (isset($rota[2])) ? $rota[2] : null ;

// echo URL_BASE.'<br>';
// echo URL_BASE.'<br>';
// print_r($rota);

include 'src'.DS.'Model'.DS.$module . '.php';
include 'src'.DS.'Controller'.DS.$module . '.php';

if (!Request::isAjax()){
    echo 'Aplicação feita pra consultas AJAX!';
}
