-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15-Mar-2018 às 03:19
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linkya`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `projects`
--

CREATE TABLE `projects` (
  `projects_id` int(11) NOT NULL,
  `title` varchar(140) NOT NULL,
  `create_date` date NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projects`
--

INSERT INTO `projects` (`projects_id`, `title`, `create_date`, `user`) VALUES
(1, 'Projeto Teste 001', '2018-03-14', 1),
(2, 'projeto teste 02', '2018-03-14', 1),
(5, 'test', '2018-03-14', 2),
(6, 'projeto teste 03', '2018-03-14', 1),
(27, 'projeto teste 04', '2018-03-14', 1),
(28, 'projeto teste 05', '2018-03-14', 1),
(30, 'projeto teste 05', '2018-03-14', 1),
(31, 'projeto teste 07', '2018-03-14', 1),
(34, 'Último Teste', '2018-03-15', 19);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tasks`
--

CREATE TABLE `tasks` (
  `tasks_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tasks`
--

INSERT INTO `tasks` (`tasks_id`, `description`, `create_date`, `end_date`, `project`) VALUES
(1, 'teste de tarefa 01', '2018-03-14', NULL, 5),
(2, 'teste de tarefa 02', '2018-03-14', NULL, 2),
(3, 'teste', '2018-03-14', '2018-03-15', 1),
(4, 'teste', '2018-03-14', '2018-03-15', 1),
(5, 'teste', '2018-03-14', '2018-03-15', 31),
(6, 'teste final', '2018-03-15', NULL, 1),
(8, 'teste projeto 03', '2018-03-15', '2018-03-15', 6),
(13, 'Primeiro item', '2018-03-15', '2018-03-15', 33),
(14, 'Segundo item da noite', '2018-03-15', NULL, 33),
(16, 'Última tarefa da noite', '2018-03-15', '2018-03-15', 34);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `nome` char(70) NOT NULL,
  `email` varchar(255) NOT NULL,
  `usuario` char(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `nome`, `email`, `usuario`, `password`) VALUES
(1, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'renan', '$2y$12$EyNeFPUDjgrLHQTJ6DQKCuKBPWO1mcaMjR1eCYg9VelbnMRlnHAWG'),
(3, 'Bárbara Ferreira de Souza Silva', 'babi_itsok@hotmail.com', 'babi', '$2y$12$IaPEmF5pzmGnRxOxCFPYwuNnxwrbUw.8rQX8eC6W3Kv94HIGkJuYS'),
(4, 'Nome Teste', 'teste@teste.com', 'teste', '$2y$12$xRoixdA990Raz3rFOjvR1.OnFFVB1xxID1633LhVUUYUDMu0GYo1i'),
(5, 'Nome Teste', 'teste@teste.com', 'teste', '$2y$12$knLSZh4UE.zXtx01y1Zidu2GK7sJRlAemJE5FdjLRi/I7RJ3pRWne'),
(6, 'Nome Teste', 'teste@teste.com', 'teste', '$2y$12$5o0YV/VwdADOBjXS5XLl9ePhiC4I23Rtf.XtOek39T3I/G/eAKacG'),
(8, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'asdasd', '$2y$12$1t0LZ/tmFmsJHbPq5V.unOBjD7EKY7NnaHsDacQHvrnLnjN7eJ/lO'),
(9, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'sdasa', '$2y$12$2WyoQ9X8jpgWlikjFXPjoeQxtchy8324pbcC66DN7vvNoy3BhYJJO'),
(10, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'asdasd', '$2y$12$0DHooT1rXo/3O4XYp0WsveqbtPgAhtIY7UlCvx82Ix4djNXaVAY0a'),
(11, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'asdasdsdsdsd', '$2y$12$a6RFuH7MxPYLJTubcI0nc.2YJBOOeVFnjDmeD/ADndSnBOnO5TDxa'),
(12, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'renan', '$2y$12$O8xIGJg7FOcSOFm71mngY.XINhRYmEqxkLMod.VL0ZB0xoWFar4kO'),
(13, 'Renan Pantoja Vilas', 'renan.pantoja@hotmail.com', 'easdsdsd', '$2y$12$Ie4PlhqdnVBZWOv1/dGk7OSf6RLL4R23Pd8x7TIRgaZflV/jbkv5.'),
(14, 'Nome Teste', 'teste@teste.com', 'teste02', '$2y$12$6DoAWxCPPhwpvnffvjvdIut5m.biFrfVP6u72TL1jvb2mVdRG3CQa'),
(15, 'teste03', 'teste@teste.com', 'teste03', '$2y$12$s7KCT6NoAz7lQbRSVyETAeN07eWxSqkfbeuVf0uWzt3aBSoTh/UJC'),
(16, 'teste04', 'teste@teste.com', 'teste04', '$2y$12$lAHfRQewH8UqpwTHylYQ3u7GyK1j4G50koM9mXpFTZ7X8rCM8Ke0e'),
(17, 'jamel', 'jamel@test.com', 'jamel', '$2y$12$5bbZ8SKfu7CXeR3cTsfUVezGXAeu4ia5.mzh/szrGTjHXCuPmV.b.'),
(18, 'testando', 'teste@testando', 'testando', '$2y$12$4RO4XvBvfzylHObYt0CDYOhiNT2fg7EgFVwEMt3stM9GtUPdCGiEW'),
(19, 'luis vianna', 'luis@vianna.com', 'luis', '$2y$12$uO34vJtP5ka0aE3MY1Xp/./et8WB4BowH5.hmoQSHuGXr7ZyCMwjG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projects_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`tasks_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `projects_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `tasks_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
